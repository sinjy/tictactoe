class game {
    constructor() {
        this.boxes = [];

        //Récupération de la div avec un id 'game'
        let boardGame = document.querySelector('#game');
        
        //Création de la section avec la class 'grid'
        this.grid = document.createElement('section');
        this.grid.setAttribute('class', 'grid');
        //Ajout de la grid au jeu
        boardGame.appendChild(this.grid);

        //Initilisation du tour
        this.turn = 0;

        this.count= 0;

        this.players = ["✖️", "⭕"];

        this.winningPositions = [
            [1,2,3], [1,5,9], [1,4,7], [2,5,8], [3,6,9], [3,5,7], [7,8,9], [4,5,6]
        ];


        //création du plateau de jeu pour chaque item du tableau boxArray
        for(let i = 0; i < 9; i++) {
            //création de la div d'une box avec la class 'box'
            let box = document.createElement('div');
            box.classList.add('box');
            //insertion de la box à la grid
            this.grid.appendChild(box);

            this.boxes.push(box);
        }
    }
    detectWin(){
        //Une fonction qui verifie si la case a été cliqué et lui ajoute un data-dejaClique = 1
        this.winningPositions.forEach(winningPosition => {
            let aligned = [0, 0];

            this.boxes.forEach((box, i) => {
                if(box.innerHTML === "" || !winningPosition.includes(i+1)) {
                    return;
                }

                console.log(box.innerHTML, i );

                if(box.innerHTML === "✖️") {
                    aligned[0]++;
                }
                else {
                    aligned[1]++;
                }
            }); 

            console.log(winningPosition, aligned, this.turn);
            console.log(this.count);

            if(aligned[0] === 3 || aligned[1] === 3) {
                this.end()
                
            }
            else if(!winningPosition || this.count === 8){
                this.draw()
            }
        });
    }

    end() {
        alert("Victoire !");
        this.boxes.forEach(box => {
            box.innerHTML = "";
            this.count = -1;
        })
    }

    draw() {
        alert("Match nul !")
        this.boxes.forEach(box => {
            box.innerHTML = "";
            this.count = -1;
    })
}


    start() {
        

        this.grid.addEventListener('click', event => {
            let boxClicked = event.target;

            console.log("-"+boxClicked.innerHTML+"-")

            if(boxClicked.innerHTML) {
                alert("🤣🤣NEWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWBIE!!!!!!!!🤣🤣\n        Ba alors on connait plus les rêgles, clique sur une autre case voyons ! 🤪")
                
            }else {
                boxClicked.innerHTML = this.players[this.turn]

                this.detectWin();

                this.nextTurn();

                this.count++;
            }

            

        });
            
    
    }

    nextTurn() {
        if(this.turn === 0) {
            this.turn = 1;
        }
        else {
            this.turn = 0;
        }

        this.turn == (this.turn === 0) ? 1 : 0 ;
    }
}

let letsPlay = new game;
letsPlay.start();